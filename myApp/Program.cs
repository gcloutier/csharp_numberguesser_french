﻿using System;

namespace myApp
{
    class Program
    {
        static void Main(string[] args)
        {

            GetAppInfo(); // run getappinfo funtion to to know infos

            GreetUser(); // greet and get users infos

            while(true){

            // create a random objet
            Random random = new Random();

            // Init correct numbers
            int correctNumber = random.Next(1,10);

            // Init guess var
            int guess = 0;

            //  Ask users for numbers
            Console.WriteLine("Trouvez un nombre entre 1 et 10");

            // while guest numers is not correct
            while(guess != correctNumber){

                // get users input
                string inputNumber = Console.ReadLine();

                // make sure its a number
                if(!int.TryParse(inputNumber, out guess)){

                    // Print error msg
                    PrintColorMessage(ConsoleColor.Red, "Ceci n'est pas un nombre, entrez un nombre.");

                    // keep going
                    continue;
                }
                // cast to int and put it in guess
                guess = Int32.Parse(inputNumber);

                // Match guess to correct number
                if(guess != correctNumber){

                    // Print error msg
                    PrintColorMessage(ConsoleColor.Red, "Mauvais nombre, essayez de nouveau...");
                }
            }

            // Output success message

                    // Print success msg
                    PrintColorMessage(ConsoleColor.Yellow, "Merveilleux, vous avez trouvé le bon nombre!");

                    // ask user play again
                    Console.WriteLine("Rejouer? [Y or N]");

                    // Get answer
                    string answer = Console.ReadLine().ToUpper();

                    if(answer == "Y"){
                        continue;
                    }
                    else if(answer == "N"){
                        return;
                    }
                    else{
                        return;
                    }

            }
        }

        // get and display app infos
        static void GetAppInfo(){
            // Set app variables
            string appName = "Number Guesser";
            string appVersion = "1.0.0";
            string appAuthor = "Guillaume Cloutier";

            // Change text color
            Console.ForegroundColor = ConsoleColor.Green;

            // Write out app info
            Console.WriteLine("{0}: Version {1} by {2}",appName,appVersion,appAuthor);

            // Reset Console
            Console.ResetColor();
        }

            // greet and know user infos
        static void GreetUser(){
            // Ask Users Name
            Console.WriteLine("Quel est votre nom?");

            // Get User input
            string inputName = Console.ReadLine();

            Console.WriteLine("Bonjour {0}, jouons à un jeu....",inputName);
        }

        // Print color message
        static void PrintColorMessage(ConsoleColor color, string message){
            // Change text color
                    Console.ForegroundColor = color;

                    // tell users wrong number
                    Console.WriteLine(message);

                    // Reset Console
                    Console.ResetColor();
        }
    }
}

